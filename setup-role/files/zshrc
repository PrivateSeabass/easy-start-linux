# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
bindkey -e
# End of lines configured by zsh-newuser-install

# zsh theme
source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme

# The following lines were added by compinstall
zstyle :compinstall filename '/home/{{@@ user @@}}/.zshrc'

autoload -Uz compinit
autoload edit-command-line
zle -N edit-command-line

compinit
# End of lines added by compinstall

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.plugin.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh


# Completion for kitty
kitty + complete setup zsh | source /dev/stdin

# Fix for SSH terminal troubles (https://github.com/kovidgoyal/kitty/issues/879)
alias ssh="kitty +kitten ssh"


#########################################
# This dotfile is managed using dotdrop #
#########################################


setopt EXTENDED_HISTORY          # Write the history file in the ":start:elapsed;command" format.
setopt INC_APPEND_HISTORY        # Write to the history file immediately, not when the shell exits.
setopt SHARE_HISTORY             # Share history between all sessions.
setopt HIST_EXPIRE_DUPS_FIRST    # Expire duplicate entries first when trimming history.
setopt HIST_IGNORE_DUPS          # Don't record an entry that was just recorded again.
setopt HIST_IGNORE_ALL_DUPS      # Delete old recorded entry if new entry is a duplicate.
setopt HIST_FIND_NO_DUPS         # Do not display a line previously found.
setopt HIST_IGNORE_SPACE         # Don't record an entry starting with a space.
setopt HIST_SAVE_NO_DUPS         # Don't write duplicate entries in the history file.
setopt HIST_REDUCE_BLANKS        # Remove superfluous blanks before recording entry.
setopt interactivecomments       # Allow typing comments at an interactive prompt

#Commands to run on new shell
bindkey -e

#PS1='[user@localhost %1~] %# ' # Only show 1 level of the path
PS1='
╭─[user@localhost %~]
╰─%# '



setopt prompt_subst
autoload -Uz vcs_info
zstyle ':vcs_info:*' actionformats \
      '%F{5}(%f%s%F{5})%F{3}-%F{5}[%F{2}%b%F{3}|%F{1}%a%F{5}]%f '
zstyle ':vcs_info:*' formats       \
      '%F{5}(%f%s%F{5})%F{3}-%F{5}[%F{2}%b%F{5}]%f '
      zstyle ':vcs_info:(sv[nk]|bzr):*' branchformat '%b%F{1}:%F{3}%r'

      zstyle ':vcs_info:*' enable git cvs svn

      # or use pre_cmd, see man zshcontrib

vcs_info_wrapper() {
  vcs_info
  if [ -n "$vcs_info_msg_0_" ]; then
    echo "%{$fg[grey]%}${vcs_info_msg_0_}%{$reset_color%}$del"
  fi
}
RPROMPT="$(vcs_info_wrapper)"


export PATH=$PATH:~/.npm-global/bin:~/bin:/bin/scripts:~/.local/bin/
export EDITOR=nvim
export VISUAL=nvim
export DOTDROP_PROFILE=laptop
export FZF_DEFAULT_OPTS="-m --preview 'bat --style=numbers --color=always {} 2>/dev/null'"


#Aliases
alias wifi-menu='sudo wifi-menu -o' #-o -> shows asterisks in place of passwords
alias battery='upower -i $(upower -e |grep 'BAT') | grep -E "state|to\ full|percentage"' #Displays battery percentage from terminal
#alias torbrowser='tor-browser-en&' #Launches TOR browser in background
#alias javac='javac -Xdiags:verbose'
#alias gcj='gcj-6 -C -ftarget=1.5'
#alias gij='gij-6'
#alias ytdl='youtube-dl -f m4a'
#alias tmux='tmux -u'
alias du='dua'
alias myip='curl ifconfig.co'
alias vim='nvim'
alias ls='exa --classify --group-directories-first --color-scale'
alias la='exa --all --classify --group-directories-first --color-scale'
alias ll="exa --long --all --all --color-scale"
alias lsl="exa --long --classify --all | less"
#alias pacman='sudo pacman'
alias rm='rm -Iv'            # -I -> Prompt only if more than 3 files, -v -> Verbose mode
#alias xterm='xfce4-terminal' # Launches the xfce4 terminal
#alias mkexec='chmod +x'      # Add exec rights to a file
alias shred='shred --remove --zero --iterations=4' #Overwrites a file with zeros, then removes it
alias info='info --vi' # Puts info into vi mode so hjkl are movement keys
alias bc='bc -ql'      # -q -> don't show prompt, -l -> load std math lib; allows FLOP
alias rmorphan='sudo pacman -Rns $(pacman -Qtdq)'                 # Remove orphan packages
#alias dotdrop='/usr/bin/dotdrop --cfg=~/dotdrop/config.yaml' # Call dotdrop with correct config
#alias nvidia-settings="optirun -b none nvidia-settings -c :8 "
#alias nvidia-on="sudo system76-power graphics power on ; sudo modprobe nvidia-drm nvidia-modeset nvidia"
#alias nvidia-off="sudo rmmod nvidia-drm nvidia-modeset nvidia ; sudo system76-power graphics power off"

#[ -f ~/.config/cani/completions/_cani.zsh ] && source ~/.config/cani/completions/_cani.zsh # caniuse terminal support

# Creates an archive (*.tar.gz) from given directory.
function maketar() { tar cvzf "${1%%/}.tar.gz"  "${1%%/}/"; }

# Create a ZIP archive of a file or folder.
function makezip() { zip -r "${1%%/}.zip" "$1" ; }

# Setup the extract command
function extract {
 if [ -z "$1" ]; then
    # display usage if no parameters given
    echo "Usage: extract <path/file_name>.<zip|rar|bz2|gz|tar|tbz2|tgz|Z|7z|xz|ex|tar.bz2|tar.gz|tar.xz>"
 else
    if [ -f $1 ] ; then
        #NAME=${1%.*}
	#mkdir $NAME && cd $NAME
        case $1 in
          *.tar.bz2)   tar xvjf $1    ;;
          *.tar.gz)    tar xvzf $1    ;;
          *.tar.xz)    tar xvJf $1    ;;
          *.lzma)      unlzma $1      ;;
          *.bz2)       bunzip2 $1     ;;
          *.rar)       unrar x -ad $1 ;;
          *.gz)        gunzip $1      ;;
          *.tar)       tar xvf $1     ;;
          *.tbz2)      tar xvjf $1    ;;
          *.tgz)       tar xvzf $1    ;;
          *.zip)       unzip $1       ;;
          *.Z)         uncompress $1  ;;
          *.7z)        7z x $1        ;;
          *.xz)        unxz $1        ;;
          *.exe)       cabextract $1  ;;
          *)           echo "extract: '$1' - unknown archive method" ;;
        esac
    else
        echo "$1 - file does not exist"
    fi
fi
}

[[ $- == *i* ]] && source "/usr/share/fzf/completions.zsh" 2> /dev/null
source "/usr/share/fzf/key-bindings.zsh"


# zsh keyboard oddity fixes
zstyle ':completion:*:*:*:default' menu yes select search #add interactive and searchable menu to tab completions
bindkey "^[[3~" delete-char # DELETE key removes character in front (opposite of backspace)
bindkey \^U backward-kill-line # CTRL + U delets all to left        
bindkey "^[[1;5C" forward-word # CTRL + right arrow key jump
bindkey "^[[1;5D" backward-word # CTRL + left arrow key jump
bindkey '^[[Z' autosuggest-accept # Accept suggestion from zsh-autosuggest
bindkey '^ ' autosuggest-execute # Execute suggestion from zsh-autosuggest
bindkey "$terminfo[kcuu1]" history-substring-search-up # Up arrow key searches for similar commands to what was typed
bindkey "$terminfo[kcud1]" history-substring-search-down # Down arrow key searches for similar commands to what was typed

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh


archey
upower -i $(upower -e |grep 'BAT') | grep -E "state|to\ full|percentage"
