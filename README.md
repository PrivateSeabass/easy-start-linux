# Easy Start Linux

I flash Linux a lot.  
This script will make the process easier for me, any time I want to flash it again.  
I also take suggestions for nice other applications people find, and add them to the list.  

Created by Private SeaBass.  
Gitlab:  
 https://gitlab.com/PrivateSeaBass/easy-start-linux  

This is a basic setup script for setting up a debian based computer  
Part of the purpose is to allow for someone who  
 would like an easier personalizable startup for Linux.  
In light of that:  
 grab this script,  
 rename it,  
 and store it with personally prefered basic applications for any time you flash Linux!  

 These applications are known to work in the current Debian & the current Manjaro.

## ============ Quick Instructions ================

##### Ansible Option
 You only need to download the AnsibleStartupScripts folder.  
 chmod u+x executionScript.sh  
 ./executionScript.sh  
 Everything else is guided and installed based on preferences set in the main playbook (main.yml).  

##### Bash Script Option
 Pick the script based on which system your OS is based on   
 (ex: debian = Ubuntu & arch = Manjaro)  
 "#" on applications you do NOT want.   
 Remove "#" on applications you want.  
 chmod u+x SCRIPTNAME (to make it executable)  
 Execute script with "sudo".    

## ============ Full Instructions =================

##### Ansible Option

In the playbook "main.yml" open it with a text editor  
Add a "#" at start of line if unwanted application,  
Remove a "# if wanted application,  
Save the script:  

 * vim/vi  
    * :w (or to save and quit :wq)  
 * nano  
    * ^x (Control + x)  
 * GUI editor  
    * ^s (Control + s)(Usually)  

Exit text editor,  
Make the script executable  
(Terminal Command: chmod u+x EasyDebian-BasedStartup.sh )    

Run the script with root permissions (as root or with sudo):  
Sudo:  
sudo ./EasyDebian-BasedStartup.sh  
Root user:  
./EasyDebian-BasedStartup.sh  

##### Bash Script Option
 Add a "#" at start of line if unwanted application,   
 Remove a "# if wanted application,  
 Save the script:  
 
 * vim/vi      
   * :w (or to save and quit :wq)  
 * nano 		
   * ^x (Control + x)  
 * GUI editor  
   * ^s (Control + s)(Usually)  
 
 Exit text editor,  
 Make the script executable  
    (Terminal Command: chmod u+x EasyDebian-BasedStartup.sh )  
 
 Run the script with root permissions (as root or with sudo):  
 Sudo:  
  sudo ./EasyDebian-BasedStartup.sh  
 Root user:  
  ./EasyDebian-BasedStartup.sh  

 Try running again.  


Think there is something everyone should have at start,  
Or think that there is an application to apt install should be there?  
suggest it!  