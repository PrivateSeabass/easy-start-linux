#!/bin/bash

sudo cat /etc/sudoers | grep $USER > TEMP

# if not already there, add it.
if [ ! -s TEMP ] ; then
	echo "adding $USER"
	echo "$USER ALL=(ALL:ALL) ALL" | sudo EDITOR='tee -a' visudo
else
	echo "$USER is already in sudoers"
fi

# remove temp
sudo rm -f TEMP
